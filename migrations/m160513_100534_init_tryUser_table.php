<?php

use yii\db\Migration;

class m160513_100534_init_tryUser_table extends Migration
{
  public function up()
    {
		$this->createTable(
		'tryUser',
			[
				'id' => 'pk',
				'username' => 'string',
				'password' => 'string',
				'auth_key' => 'string'
			]
		);
    }

    public function down()
    {
		$this->dropTable('tryUser');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}


