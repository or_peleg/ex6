<?php

use yii\db\Migration;

class m160510_183834_init_user_table extends Migration
{
      public function up()
    {
		$this->createTable(
		'user',
			[
				'id' => 'pk',
				'username' => 'string',
				'password' => 'string',
				'auth_key' => 'string'
			]
		);
    }

    public function down()
    {
		$this->dropTable('user');
    }

}
