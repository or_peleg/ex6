<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnLeadRule extends Rule
{
	public $name = 'ownLeadRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) { 
			return isset($params['user']) ? $params['user']->id == $user : false;
		}
		return false;
	}
}