<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class LeadupdateController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnLead = $auth->getPermission('updateOwnLead');
		$auth->remove($updateOwnLead);
		
		$rule = new \app\rbac\OwnLeadRule;
		$auth->add($rule);
				
		$updateOwnLead->ruleName = $rule->name;		
		$auth->add($updateOwnLead);	
	}
}
