<?php

namespace app\models;
use \yii\db\ActiveRecord;

class Student extends ActiveRecord
{
    public $id;
    public $name;
	
	public static function tableName()
	{
		return 'students';
	}
	
	public function getFullName()
	{
		return $this->getAttribute('name');
	}
}
