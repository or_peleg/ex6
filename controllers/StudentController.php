<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;

class StudentController extends Controller
{
    public function actionView($id)
    {
        $student = Student::findOne($id);
		$name = $student->getFullName($id);
		return $this->render('showOne' , ['name' => $name]);
    }
}


